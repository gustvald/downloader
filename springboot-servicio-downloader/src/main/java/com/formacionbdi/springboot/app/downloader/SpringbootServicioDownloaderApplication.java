package com.formacionbdi.springboot.app.downloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringbootServicioDownloaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioDownloaderApplication.class, args);
	}

}
